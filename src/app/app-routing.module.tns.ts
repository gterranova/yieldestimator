// angular
import { NgModule } from '@angular/core';
import { NativeScriptRouterModule as RouterModule } from 'nativescript-angular';
// app
import { AppRoutes } from './app.routes';

@NgModule({
    imports: [
        RouterModule.forRoot(<any>AppRoutes),
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }


