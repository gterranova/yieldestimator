// src/app/app-toolbar/app-toolbar.service.ts
import { Injectable } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
 
export interface MenuItem {
    path: string;
    title: string;
    icon?: string;
    actions?: MenuItem[];
}
 
@Injectable()
export class AppToolbarService {
    // Make it possible to send an event about menu changed, so we can talk between components
    activeMenuItem$: BehaviorSubject<MenuItem> = new BehaviorSubject<MenuItem>({path:'', title:''});

    constructor(private router: Router, private titleService: Title) {
        this.router.events
            .filter(e => e instanceof NavigationEnd)
            .map(_ => this.router.routerState.root)
            .subscribe(route => {
                let active = this.lastRouteWithMenuItem(route.root);
                //if (active && active.title) {
                //  this.titleService.setTitle(active.title);
                //}
                this.activeMenuItem = active;
            });
    }

    get activeMenuItem() {
        return this.activeMenuItem$.asObservable();
    }

    set activeMenuItem(menu: any) {
        this.activeMenuItem$.next(menu);
    }

    get title() {
        return this.activeMenuItem$.getValue().title;
    }
    set title(title : string) {
        let active = this.activeMenuItem$.getValue();
        active.title = ''+title;
        this.activeMenuItem = active;
    }
    getMenuItems(): MenuItem[] {
        return this.router.config
            //only add a menu item for routes with a title set.
            .filter(route => route.data && route.data.title && !route.data.hidden ) 
            .map(route => {
                return {
                    path: route.path,
                    title: route.data.title,
                    icon: route.data.icon
                };
            });
    }

    private lastRouteWithMenuItem(route: ActivatedRoute): MenuItem {
        let lastMenu = undefined;
        do { lastMenu = this.extractMenu(route) || lastMenu; }
        while ((route = route.firstChild));
        return lastMenu;
    }
    private extractMenu(route: ActivatedRoute): MenuItem {
        let cfg = route.routeConfig;
        return cfg && cfg.data
            ? { path: cfg.path, title: cfg.data.title||'', icon: cfg.data.icon, actions: cfg.data['actions']||[] }
            : undefined
    }
}