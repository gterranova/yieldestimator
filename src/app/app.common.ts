import { AppRoutingModule } from './app-routing.module';
// demo
import { HomeModule } from './home/home.module';
import { AboutModule } from './+about/about.module';
import { YawyerModule } from './+yawyer/yawyer.module';

export const SHARED_MODULES: any[] = [
    AppRoutingModule,
    HomeModule,
    AboutModule,
    YawyerModule
];

export * from './app-routing.module';
