import { Routes } from '@angular/router';
// app
import { HomeComponent } from './components/home/home.component';
import { AboutRoutes } from '../+about/about.routes';
import { YawyerRoutes } from '../+yawyer/yawyer.routes';

export const HomeRoutes: Routes = [
    {
        path: 'home',
        component: HomeComponent,
        data: {
            title: 'Yawyer',
            icon: false
        }
    },
    {
        path: 'about',
        children: AboutRoutes,
        data: {
          title: 'About',
          icon: 'arrow_back'
        }
        //loadChildren: 'app/+about/about.module#AboutModule'
    },
    {
        path: 'browse',
        children: YawyerRoutes,
        data: {
          title: 'Browse',
          icon: 'arrow_back',
          actions: [
              { title: '', icon: 'shopping_cart', path: '/layouts' }
          ],
        }
        // FIX: Workaround for https://github.com/angular/angular-cli/issues/9825
        //loadChildren: 'app/+yawyer/yawyer.module#YawyerModule'
    }
];
