import { NgModule } from '@angular/core';
import { SHARED_MODULES, COMPONENT_DECLARATIONS } from './about.common';
import { MaterialModule } from '../material.module';

@NgModule({
    imports: [
        MaterialModule,
        ...SHARED_MODULES,
    ],
    declarations: [
        ...COMPONENT_DECLARATIONS
    ],
})
export class AboutModule { }
