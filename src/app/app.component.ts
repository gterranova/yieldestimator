import { Component, Input } from '@angular/core';
import { Location } from '@angular/common';
// vendor dependencies
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';
import { AppToolbarService, MenuItem } from './app-toolbar.service';

declare const require: any;

@Component({
    moduleId: module.id,
    selector: 'maestro-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']    
})
export class AppComponent {
    @Input() title: string = "Yield Estimator";
    mainMenuItems;
    activeMenuItem$: Observable<MenuItem>;

    constructor(translate: TranslateService, 
        private location: Location,
        private menuService: AppToolbarService) {
        translate.setTranslation('en', require('../assets/i18n/en.json'));
        translate.setDefaultLang('en');
        translate.use('en');
        this.activeMenuItem$ = menuService.activeMenuItem;
        this.activeMenuItem$.subscribe( () => this.mainMenuItems = menuService.getMenuItems());
    }

    onNavBtnTap() {
        this.location.back();
    }    
}
