import { CommonModule } from '@angular/common';
// vendor dependencies
import { TranslateModule } from '@ngx-translate/core';
// app
import { SharedModule } from '../shared';
import { RouterModule } from '../common';
import { YawyerRoutes } from './yawyer.routes';
import { YawyerComponent } from './components/yawyer/yawyer.component';
import { WindDataForm, CustomWEMDialog } from './components/wind-data-form/wind-data-form.component';
import { WecList } from './components/wec-list/wec-list.component';
import { WecPowerCurveChart } from './components/wec-power-curve-chart/wec-power-curve-chart.component';
import { WecDetailComponent } from './components/wec-detail/wec-detail.component';
import { WecPropertiesComponent } from './components/wec-detail/wec-properties.component';
import { LayoutListComponent, LayoutDetailComponent, 
         LayoutWecListComponent, LayoutWecDetailComponent, 
         LayoutCostComponent } from './components/wf-configurator';

export const SHARED_MODULES: any[] = [
    CommonModule,
    SharedModule,
    RouterModule.forChild(<any>YawyerRoutes),
    TranslateModule.forChild(),
];

export const COMPONENT_DECLARATIONS: any[] = [
    YawyerComponent,
    WindDataForm,
    WecList,
    WecPowerCurveChart,
    WecDetailComponent,
    LayoutListComponent,
    LayoutDetailComponent,
    LayoutWecListComponent,
    LayoutWecDetailComponent,
    LayoutCostComponent,
    WecPropertiesComponent,
];

export const COMPONENT_EXPORTS: any[] = [
    YawyerComponent,
    WindDataForm,
    WecList,
    WecPowerCurveChart,
    WecDetailComponent,
    LayoutListComponent,
    LayoutDetailComponent,
    LayoutWecListComponent,
    LayoutWecDetailComponent,
    LayoutCostComponent,
    WecPropertiesComponent,
];