import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/throttleTime';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/find';
import 'rxjs/add/operator/switchMap';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Wec, Layout, Settings } from '../models';
import { defaultSettings } from '../models/settings';

@Injectable()
export class DataService{
  private _formSettings: BehaviorSubject<Settings>;
  private _starredWecs: BehaviorSubject<Array<Wec>> = new BehaviorSubject([]);
  private _allWecs: BehaviorSubject<Array<Wec>> = new BehaviorSubject([]);
  private _allLayouts: BehaviorSubject<Array<Layout>> = new BehaviorSubject([]);

  private filter: any;

  constraints: any = {};
  wecsSelected: Array<number> = new Array<number>();

  constructor(private http: Http){
    let settings = localStorage.getItem('settings');
    this._formSettings = new BehaviorSubject(settings ? JSON.parse(settings) : defaultSettings);
    this.settings.throttleTime(1000).subscribe( settings => localStorage.setItem('settings', JSON.stringify(settings)));
    //this._formSettings = new BehaviorSubject(this.initialParams);
    let starredWecs = localStorage.getItem('starred');
    this.wecsSelected = starredWecs ? JSON.parse(starredWecs) : [];
    this.starredWecs.throttleTime(1000).subscribe( starred => {
      localStorage.setItem('starred', JSON.stringify(this.wecsSelected))
    });

    let filter = localStorage.getItem('filter');
    this.filter = filter ? JSON.parse(filter) : {};

    this.allLayouts = [];
    this.settings.subscribe( newSettings => this.applySettings(newSettings) );
  }

  get settings() {
    return this._formSettings.asObservable();
  }

  set settings(newSettings: any) {
    this._formSettings.next(newSettings);
  }

  getFilter() {
    return this.filter;
  }

  setFilter(filter: any) {
    localStorage.setItem('filter', JSON.stringify(filter));
    this.filter = filter;
  }

  get allWecs() {
    return this._allWecs.asObservable();
  }

  set allWecs(wecs : any) {
    this._allWecs.next(wecs);
  }

  get starredWecs() {
    return this._starredWecs.asObservable();
  }

  toggleStarredWec(wec : Wec) {
    wec.favourite = !wec.favourite;
    (wec.favourite) ? this.addStarredWec(wec) : this.removeStarredWec(wec);

  }
  
  addStarredWec(wec : Wec) : Observable<Array<Wec>> {
    this.wecsSelected.push(wec.id);
    let starredWecs = this._starredWecs.getValue();
    this._starredWecs.next([...starredWecs, wec]);
    return this._starredWecs;    
  }

  removeStarredWec(wec: Wec) : void {
    this.wecsSelected.splice(this.wecsSelected.indexOf(wec.id), 1);
    let starredWecs = this._starredWecs.getValue();
    starredWecs.splice(starredWecs.indexOf(wec), 1);
    this._starredWecs.next([...starredWecs]);  
  }

  getWec(id: number) : Observable<Wec> {
    if (this._allWecs.getValue().length==0) {
        return this.applySettings(this._formSettings.getValue())
          .map( allWecs => allWecs.find(wec => wec.id === id) );
    }
    return Observable.of(this._allWecs.getValue().find((wec: Wec) => wec.id === id));
  }

  removeAllSelectedWecs() {
    this.wecsSelected.splice(0, this.wecsSelected.length);
  }

  applySettings(settings: Settings)  : Observable<Array<Wec>> {
    let wecs = this._allWecs.getValue();
    //console.log(`Call applySettings ${wecs}`);
    if (wecs.length==0)
      return this.fetchResourcesAndApplySettings(settings);
    wecs.map( (wec) => {
      if (wec._hubHeight === wec._params.height)
        wec._hubHeight = settings.height;
      wec._params = settings;
    })    
    this._allWecs.next(wecs);
    return this.allWecs;
  }

  fetchResourcesAndApplySettings(settings: Settings) : Observable<Array<Wec>> {
    let called = 0;
    this.getResource('wecs')
      .map( wecData => wecData.map( data => Object.assign(new Wec(settings), data)) )
      .subscribe( (allWecs) => {
        //console.log(`Call no. ${called++}`);
        this.wecsSelected.map( idx => allWecs[idx-1].favourite = true );
        this._starredWecs.next(this.wecsSelected.map( (idx) => allWecs[idx-1]));
        this._allWecs.next(allWecs)
      });
    return this._allWecs;
  }

  getResource(res: string): Observable<any> {
    return this.http.get(`assets/${res}.json`)
      .map((res:any) => res.json());
  }    

  get allLayouts() {
    return this._allLayouts.asObservable();
  }

  set allLayouts(layouts : any) {
    this._allLayouts.next(layouts);
  }

  getLayout(id: number) : Observable<Layout> {
    return this.allLayouts.map( layouts => layouts.find(layout => layout.id === id) );
  }

  updateLayout(id: number, updated: Layout) : void {
    Observable.zip(this.allLayouts, this.getLayout(id)).first().subscribe( args => {
      Object.assign(args[1], updated);
      this._allLayouts.next(<Array<Layout>>args[0]);
      });
  }

  addLayout() : Observable<Layout> {
    let layouts = this._allLayouts.getValue();
    let newLayout = new Layout(layouts.length+1);
    this._allLayouts.next([...layouts, newLayout]);
    return Observable.of(newLayout);    
  }

  removeLayout(layout: Layout) : void {
    let layouts = this._allLayouts.getValue();
    layouts.splice(layouts.indexOf(layout), 1);
    this._allLayouts.next([...layouts]);  
  }

}