let calcAlpha = (form) => ((Math.log(form.v2.value/form.vRef.value))/(Math.log(form.z2.value/form.zRef.value)));

let powerLaw = (vRef: number, zRef: number, z: number, alpha: number) => vRef*(Math.pow((z/zRef),alpha));

let logLaw = (uRef: number, hRef: number, h2: number, z: number) => uRef*((Math.log(h2/z))/(Math.log(hRef/z)));

let weibull = (x: number, options: { k:number, lambda:number}) => {
    let k = options.k, lambda = options.lambda;
    if (x <= 0) {
        return 0;
    }
    //console.log(x, (k/Math.pow(lambda, k)) * Math.pow(x, k - 1) * Math.exp(-Math.pow( x / lambda, k))));
    return (k/Math.pow(lambda, k)) * Math.pow(x, k - 1) * Math.exp(-Math.pow( x / lambda, k));
};

let gamma = (n) => {  // accurate to about 15 decimal places
  //some magic constants 
  var g = 7, // g represents the precision desired, p is the values of p[i] to plug into Lanczos' formula
      p = [0.99999999999980993, 676.5203681218851, -1259.1392167224028, 771.32342877765313, -176.61502916214059, 12.507343278686905, -0.13857109526572012, 9.9843695780195716e-6, 1.5056327351493116e-7];
  if(n < 0.5) {
    return Math.PI / Math.sin(n * Math.PI) / this.gamma(1 - n);
  }
    n--;
    var x = p[0];
    for(var i = 1; i < g + 2; i++) {
      x += p[i] / (n + i);
    }
    var t = n + g + 0.5;
    return Math.sqrt(2 * Math.PI) * Math.pow(t, (n + 0.5)) * Math.exp(-t) * x;
};

export { calcAlpha, powerLaw, logLaw, weibull, gamma };

