
export interface Settings {
    vmast_hh: number;
    hmast_hh: number;
    height: number;
    k: number;
    z: number;
    wem: { type: number, value: number };
    alpha: number;
    rho: number;
    max_power: number;
    losses: number;
    availability: number;
    lambda: number;
}

let defaultSettings: Settings = {
    vmast_hh: 7.0,
    hmast_hh: 60,
    height: 70,
    k: 2.00,
    z: 0.0002,
    wem: { type: 0, value: 0.4 },
    alpha: 0.06,
    rho: 1.225,
    max_power: 0,
    losses: 7,
    availability: 100,
    lambda: 0,
}

function compareSettings(a: Settings, b: Settings) {
    return (
        a.vmast_hh == b.vmast_hh &&
        a.hmast_hh == b.hmast_hh &&
        a.height == b.height &&
        a.k == b.k &&
        a.z == b.z &&
        a.wem.type == b.wem.type &&
        a.alpha == b.alpha &&
        a.rho == b.rho &&
        a.max_power == b.max_power &&
        a.losses == b.losses &&
        a.availability == b.availability &&
        a.lambda == b.lambda
        );
}

export { defaultSettings, compareSettings };