import { CostType, Countries, Currencies, countrySelect, currencySelect } from '../consts';
import { Wec } from './wec';

export class WecGroup {
    wec: Wec;
    count: number;

    constructor(wec: Wec, count: number = 1) {
        this.wec = wec;
        this.count = count;
    }
    hasWec(wec: Wec) {
        return this.wec.equals(wec);
    }

    totalKey(key: string) {
        return this.wec[key]*this.count;
    }

    toString() : string {
      return `${this.count.toString()}x${this.wec.model}@${this.wec.hubHeight}m`;
    }
}

export interface CostItem {
  type: CostType;
  amount: number;
  periodRange: Array<number>;
}

export class Layout {
  id: number;
  startOperation: number = 2018;
  period: number = 20;
  currency: Currencies = Currencies.EUR;
  discountRate: number = 0;
  wecs: Array<WecGroup> = new Array<WecGroup>();
  costItems: Array<CostItem> = new Array<CostItem>();

  constructor(id) {
    this.id = id;
  }

  get currencyLabel() {
    return Currencies[this.currency];
  }
  
  addWec(wec: Wec) {
      let wecGroup = this.wecs.find( (wg) => wg.hasWec(wec));
      if (wecGroup) {
          wecGroup.count += 1;
      } else {
          this.wecs.push(new WecGroup(wec, 1));
      }
  }

  removeWec(wec: Wec) {
      let wecGroup = this.wecs.find( (wg) => wg.hasWec(wec));
      this.removeWecFromGroup(wecGroup);
  }

  removeWecFromGroup(wecGroup: WecGroup) {
      if (wecGroup && wecGroup.count > 1) {
          wecGroup.count = wecGroup.count - 1;
      } else if (wecGroup) {
          this.wecs.splice(this.wecs.indexOf(wecGroup), 1);
      }
  }

  get name() : string {
      if (this.wecs.length > 0) {
        return this.wecs.sort((a,b)=> a.totalKey('ratedPower')<b.totalKey('ratedPower')? 1 :
          (a.totalKey('ratedPower')==b.totalKey('ratedPower')?(a.wec.model<b.wec.model?1:-1):-1))
          .map( group => group.toString()).join(' ');
      }
      return 'Empty layout';
  }

  get numberOfWecs() : number {
    return this.wecs.reduce( (accum, current) => accum + current.count, 0);
  }

  minPower() : number {
    return this.wecs.reduce( (accum, current) => Math.min(accum, current.wec.ratedPower), 99999);
  }

  totalWecKey(key: string) : number {
    return this.wecs.reduce( (accum, current) => accum + current.totalKey(key), 0);
  }

  get deratedPower(): number {
    return this.totalWecKey('deratedPower');
  }

  get grossYield() : number {
    return this.totalWecKey('grossYield');
  }

  get netYield() : number {
    return this.totalWecKey('netYield');
  }

  get EFLH() : number {
    return Math.ceil(this.totalWecKey('netYield')/ (this.totalWecKey('ratedPower') / 1000));
  }

  get NCF() : number {
    return Math.ceil(this.totalWecKey('netYield')/ ((this.totalWecKey('ratedPower') / 1000) * 8760) * 1000) / 1000;
  }

  get NPVMWh() : number {
    let mwh = 0;
    let idx, w;
    for (idx=1; idx < this.period+1; idx++) {
      for (w=0; w < this.wecs.length; w++) {
        let item = this.wecs[w];
        mwh += (item.totalKey('netYield') / Math.pow((1+this.discountRate/100), idx));
      }
    }
    return mwh;
  }

  get LCOE() : number {
    return Math.ceil(this.costTotal/this.NPVMWh*100)/100;
  }

  get costGroupedPeriods() {
    let costData = {};
    let idx;
    for (idx=0; idx < this.costItems.length; idx++) {
      let item = this.costItems[idx];
      costData[+item.type] = costData[+item.type]||[];
    }
    for (idx=0; idx < this.period+1; idx++) {
      Object.keys(costData).forEach( key => costData[+key].push(0));
      this.costItems
        .filter( cost => ((+cost.periodRange[0] < idx) && (idx <= +cost.periodRange[1])) || 
          (+cost.periodRange[0] == +cost.periodRange[1] && +cost.periodRange[0] == idx) )
        .map( item => {
          //console.log(idx, item, this.costData[+item.type][idx]);
          costData[+item.type][idx] += parseFloat(item.amount.toString())*this.numberOfWecs / Math.pow((1+this.discountRate/100), idx);
        })  
      }  
    return costData;    
  }

  get costGroupedTotals() {
    let costData = {};
    let costGroupedTotals = this.costGroupedPeriods;
    Object.keys(costGroupedTotals).map( key => {
      costData[key] = costGroupedTotals[key].reduce( (accum, current) => accum+current, 0);
    })
    return costData;
  }

  get capexOther() : number {
    let total = 0;
    let costGroupedTotals = this.costGroupedTotals;
    Object.keys(costGroupedTotals)
      .filter( costItem => +costItem == CostType.OtherCAPEX).map( key => {
      total += costGroupedTotals[key];
    })
    return total;    
  }

  get capexTotal() : number {
    let total = 0;
    let costGroupedTotals = this.costGroupedTotals;
    Object.keys(costGroupedTotals)
      .filter( costItem => +costItem == CostType.CAPEX || +costItem == CostType.OtherCAPEX).map( key => {
      total += costGroupedTotals[key];
    })
    return total;    
  }

  get opexTotal() : number {
    let total = 0;
    let costGroupedTotals = this.costGroupedTotals;
    Object.keys(costGroupedTotals)
      .filter( costItem => +costItem == CostType.OPEX).map( key => {
      total += costGroupedTotals[key];
    })
    return total;    
  }

  get costTotal() {
    let total = 0;
    let costGroupedTotals = this.costGroupedTotals;
    Object.keys(costGroupedTotals).map( key => {
      total += costGroupedTotals[key];
    })
    return total;
  }
}
