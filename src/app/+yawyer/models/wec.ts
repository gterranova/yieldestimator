import { weibull, logLaw, powerLaw, gamma } from '../utils';
import { Settings, compareSettings } from './settings';

export class Wec {
    id: number;
    manufacturer: string;
    model: string;
    rotorDiameter: number;
    yieldCurve: Array<number>;
    windClass: string;
    turbulenceClass: string;
    ratedPower: number;
    availableHubHeights: Array<number>;

    favourite: boolean = false;

    _params: Settings;
    _hubHeight: number;

    constructor(params: any) {
        this._params = Object.assign({}, params);
        this._hubHeight = params.height;
        //console.log("WEC instance", JSON.stringify(params));
    }
    
    set params(value: any) {
        this._params = Object.assign({}, value);
        this._hubHeight = this._params.height;
    }

    set hubHeight(value: number) {
        this._hubHeight = value;
    }

    get isCustomHeight() : boolean {
        return !this.availableHubHeights.find( h => h == this._hubHeight);
    }

    get hubHeight(): number {
        return this._hubHeight;
    }

    get deratedPower(): number {
        if (this._params.max_power > 0)
            return Math.min(this.ratedPower, this._params.max_power);
        return this.ratedPower;
    }

    get velocityHubHeight(): number {
        if (this._params.wem.type == 0)
            return logLaw(this._params.vmast_hh, this._params.hmast_hh, this._hubHeight, this._params.wem.value);
        return powerLaw(this._params.vmast_hh, this._params.hmast_hh, this._hubHeight, this._params.wem.value);   
    }
    
    get normalizedVelocityHubHeight(): number {
        return this.velocityHubHeight*Math.pow(this._params.rho/1.225, 1/3);   
    }

    // Weibull scale param at measurement height
    //get Amh() {
    //    return this._params.vmast_hh/(Math.exp(Math.log(gamma(1+1/this._params.k))));
    //}   
    
    // Weibull scale param at hub height
    get Ahh() {
        return this.normalizedVelocityHubHeight/(Math.exp(Math.log(gamma(1+1/this.Khh))));
    }

    // Weibull shape param at hub height
    get Khh() {
        return this._params.k+0.008*(this.hubHeight-this._params.hmast_hh);
    }

    get extrapolatedYieldCurve(): Array<number> {
        return this.ratedYieldCurve.map( (n, i) => weibull(i, {k: this.Khh, lambda: this.Ahh})*8760);
    }

    get ratedYieldCurve(): Array<number> {
        return this.yieldCurve.map( (w) => {
            if (this._params.max_power > 0)
                //console.log(v, this._params.max_power);
                return Math.min(this._params.max_power, w);
            return w;
        });
    }

    get tipHeight(): number {
        return this._hubHeight + this.rotorDiameter / 2;
    }

    get sweptArea(): number {
        return Math.pow(this.rotorDiameter/2, 2)*Math.PI;
    }

    get grossYield() {
        let ratedYield = this.ratedYieldCurve;
        return this.extrapolatedYieldCurve.map( (w, n) => {
            return ratedYield[n]*w;
        }).reduce( (a, b) => a + b, 0)/1000;
    }

    get netYield() {
        return ((100-this._params.losses)/100)*(this._params.availability/100)* this.grossYield;
    }

    get Cp() {
        return this.ratedYieldCurve.map((e, n) => {
            if (e != 0) 
                return e/(this._params.rho/2*this.sweptArea*Math.pow(n,3))*1000;
            return 0;
        });
    }

    // Equivalent Full Load Hours (EFLH)
    get EFLH() {
        return Math.ceil(this.netYield/ (this.ratedPower / 1000));
    } 

    // Net Capacity Factor (NCF)
    get NCF() {
        return Math.ceil(this.netYield/ ((this.ratedPower / 1000) * 8760) * 1000) / 1000;
    } 

    copy() {
        let settings = Object.assign({}, this._params);
        return Object.assign(new Wec(settings), this);
    }

    equals(wec: Wec) {
        return this.id == wec.id && this._hubHeight == wec._hubHeight && compareSettings(this._params, wec._params);   
    }
}