import { Routes } from '@angular/router';
// app
import { YawyerComponent } from './components/yawyer/yawyer.component';
import { WecDetailComponent } from './components/wec-detail/wec-detail.component';
import { LayoutListComponent, LayoutDetailComponent, LayoutWecDetailComponent } from './components/wf-configurator';

export const YawyerRoutes: Routes = [
    { path: '', 
      pathMatch: 'full', 
      component: YawyerComponent,
    },
    { path: 'layouts', 
      component: LayoutListComponent,
      data: {
        title: 'Layouts',
        icon: 'arrow_back',
        hidden: true,
      }
    },
    { path: 'layouts/:id',
      component: LayoutDetailComponent,
      data: {
        title: '',
        icon: 'arrow_back',
        hidden: true,
      }
    },
    { path: 'layouts/:id/group/:group', 
      component: LayoutWecDetailComponent,
      data: {
        title: '',
        icon: 'arrow_back',
        hidden: true,
      }
    },
    { path: ':id', 
      component: WecDetailComponent,
      data: {
        title: '',
        icon: 'arrow_back',
        hidden: true,
          actions: [
              { title: '', icon: 'shopping_cart', path: '/layouts' }
          ],
        
      }
    },
];
