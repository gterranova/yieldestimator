import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { ViewChild } from '@angular/core';
import { WecPowerCurveChart } from '../wec-power-curve-chart/wec-power-curve-chart.component';
import { AppToolbarService } from '../../../app-toolbar.service';

import { Wec, Layout, Settings } from '../../models';

import { DataService } from '../../utils/data.service';

import { Observable, Subscription } from 'rxjs/Rx';

@Component({
    moduleId: module.id,
    selector: 'wec-detail-component',
    templateUrl: './wec-detail.component.html',
    styleUrls: ['./wec-detail.component.scss']
})
export class WecDetailComponent  implements OnInit, OnDestroy {
    id: number;
    wec: Observable<Wec>; 
    currentWec: Wec; 
    layouts: Observable<Array<Layout>>;
    chart : WecPowerCurveChart;
    subscriptions: Subscription[]=[];

    @ViewChild(WecPowerCurveChart) set wecPowerCurveChart(c : WecPowerCurveChart) {
        this.chart = c;
        if (this.wec)
          this.wec.first().subscribe( wec => this.chart.setupForWec(wec) );
    };

    constructor(
      private route: ActivatedRoute,
      private store: DataService,
      private snackBar: MatSnackBar,
      private menuService: AppToolbarService) {
    }

    ngOnInit() { 
        this.route.params.map( (args) => +args['id']).subscribe( id => {
            setTimeout( () => {
              this.wec = this.store.getWec(id);
              let sub1 = this.store.getWec(id).subscribe( w => {
                if (w)
                  this.menuService.title = `${w.manufacturer} ${w.model}`;
              });
              this.subscriptions.push(sub1);
            }, 0);
          });
        this.layouts = this.store.allLayouts;
    }
    ngAfterViewInit() { 
    }

    onWecSettingsChanged(wec : Wec, newSettings : Settings) {
      wec.params = newSettings;
      this.chart.setupForWec(wec);
    }

    onAddToLayout( layout: Layout, wec: Wec) {
      if (!layout) {
        this.store.addLayout().subscribe( layout => {
            layout.addWec(wec.copy());
            this.snackBar.open(`Wec ${wec.model} added to layout #${layout.id}`, 'OK', { duration: 2000 });
        });
      } else {        
        layout.addWec(wec.copy());
        this.snackBar.open(`Wec ${wec.model} added to layout #${layout.id}`, 'OK', { duration: 2000 });
      }
    }
    
    ngOnDestroy() {
      this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

}
