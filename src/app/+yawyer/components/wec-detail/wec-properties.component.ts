import { Component, OnInit, Input } from '@angular/core';
import { Wec } from '../../models';

@Component({
    moduleId: module.id,
    selector: 'wec-properties',
    templateUrl: './wec-properties.component.html',
    styleUrls: ['./wec-properties.component.scss']
})
export class WecPropertiesComponent  implements OnInit {
    @Input() wec: Wec; 

    constructor() {
    }

    ngOnInit() { 
    }

}
