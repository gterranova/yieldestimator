import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material';
import { Wec, Layout, WecGroup } from '../../models';
import { DataService } from '../../utils/data.service';
import { Observable, Subscription } from 'rxjs/Rx';

type WecGroupOrLayout = WecGroup | any;

@Component({
    moduleId: module.id,
    selector: 'wf-layout-wec-list',
    templateUrl: './wf-layout-wec-list.component.html',
    styleUrls: ['./wf-layout-wec-list.component.scss'],
})
export class LayoutWecListComponent implements OnInit, OnDestroy {
    @Input() layout: Observable<Layout>;

    displayedColumns = ['model', 'count', 'deratedPower', 'hubHeight', 'velocityHubHeight', 
      'grossYield', 'netYield', 'EFLH', 'NCF', 'edit'];
    data: MatTableDataSource<WecGroupOrLayout>;
    isLastRow = (data, index) => (index === this.data.data.length);

    subscriptions: Subscription[]=[];

    constructor(
      private store: DataService,
      private router: Router) {
    }

    ngOnInit() { 
      this.data = new MatTableDataSource<WecGroupOrLayout>([]);
      let sub1 = this.layout.subscribe( layout => {
          this.setupDataSource(layout);
      });
      this.subscriptions.push(sub1);
    }

    setupDataSource(layout: Layout) {
      if (!layout) {
        this.data.data = [];      
        return;
      }

      let totals = {
        wec: {
          manufacturer: 'Total',
          model: '',
          deratedPower: layout.deratedPower,
          hubHeight: '',
          velocityHubHeight: '',
          grossYield: layout.grossYield,
          netYield: layout.netYield,
          EFLH: layout.EFLH,
          NCF: layout.NCF
        }, 
        count: layout.numberOfWecs,
      };      
      this.data.data = [...layout.wecs, totals];
    }

    onAdjustWecCount(layout: Layout, wecGroup: WecGroup, amount: number, event: any) {
      if (event)
        event.stopPropagation();
      if (amount < 0) {
        layout.removeWecFromGroup(wecGroup);
        if (layout.wecs.length == 0) {
          // Remove layout
          this.store.removeLayout(layout);
          this.layout = null;
          return;
        }
      } else wecGroup.count += amount;
      this.setupDataSource(layout);
      this.store.updateLayout(layout.id, layout);
    }

    onEditClicked(layout: Layout, wecGroup: WecGroup, event: any) {
        this.router.navigate(['', 'layouts', layout.id, 'group', layout.wecs.indexOf(wecGroup)+1]);
    }
    
    ngOnDestroy() {
      this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

}
