import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Sort } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConstraintsDialog } from './wf-layout-constraints.dialog';
import { Wec, WecGroup, Layout, Settings } from '../../models';
import { BaseList } from '../common/base-list.component';

import { DataService } from '../../utils/data.service';

import { Observable, Subscription } from 'rxjs/Rx';
import { saveAs } from "file-saver";

@Component({
    moduleId: module.id,
    selector: 'layout-list',
    templateUrl: './wf-layout-list.component.html',
    styleUrls: ['./wf-layout-list.component.scss']
})
export class LayoutListComponent extends BaseList<Layout> implements OnInit {
    items: Observable<Array<Layout>>;
    subscriptions: Subscription[]=[];

    //displayedColumns = ['name', 'numberOfWecs', 'deratedPower', 'grossYield',
    //  'netYield', 'EFLH', 'NPVMWh', 'capexTotal', 'opexTotal', 'costTotal', 'LCOE'];

    displayedColumns = ['name', 'numberOfWecs', 'deratedPower',
      'netYield', 'EFLH', 'capexTotal', 'opexTotal', 'costTotal', 'LCOE', 'delete'];

    defaultSort: Sort = <Sort>{active: 'LCOE', direction: 'desc'};

    // MatPaginator Inputs
    length = 9;
    pageSize = 9;
    pageSizeOptions = [9, 25, 100];

    constructor(
      private router: Router,
      private store: DataService,
      private snackBar: MatSnackBar,
      public dialog: MatDialog) {
      super();
    }

    ngOnInit() { 
        this.items = this.store.allLayouts;
    }

    filterPredicate(data : Layout, filter: String) {
      return true;
    }

    sortPredicate(sort, a: Layout, b: Layout) {
      let isAsc = sort.direction == 'asc';
      if (sort.active == 'name')
        return compare(a[sort.active], b[sort.active], isAsc);
      return compare(+a[sort.active], +b[sort.active], isAsc);
    }

    onDeleteLayout(layout : Layout, event: any) {
      if (event)
        event.stopPropagation();      
      this.store.removeLayout(layout);
    }

    onSelectRow(layout: Layout) {
      this.router.navigate(['layouts', layout.id])
    }

    onHoverRow(layout: Layout) {
      
    }


    onSave(event: any) {
      var d = new Date();
      var fn = (d.getFullYear()*100 + d.getMonth()+1)*100 + d.getDate();
      var filename = `layouts_${fn}.json`;
      var blob = new Blob([JSON.stringify(this._items)], {type: "text/json;charset=utf-8"});
      saveAs( blob, filename); 
    }

    onUpload(event :any) {
      var file:File = event.target.files[0]; 
      var myReader:FileReader = new FileReader();

      let self = this;
      myReader.onloadend = function(e){
        // you can perform an action with readed data here
        let data = Array.from(JSON.parse(myReader.result));
        let layouts = data.map( (layout, idx) => {
          let wecs = Array.from(layout['wecs']);
          layout['wecs'] = wecs.map( wecGroup => {
            let wec = Object.assign(new Wec(wecGroup['wec']['_params']), wecGroup['wec']);
            return Object.assign(new WecGroup(wec, wecGroup['count']))
          });
          return Object.assign(new Layout(idx), layout);
        }); 
        self.store.allLayouts = layouts;
      }
      myReader.readAsText(file);
    }

    private randomLayout(allWecs: Array<Wec>, constraints) {
        let attempt = 0, maxAttempt = Math.pow(allWecs.length, 2);
        var layout = new Layout(-1);
        do {
            let randomNumber = Math.floor(Math.random()*allWecs.length);
            let randomWec : Wec = allWecs[randomNumber].copy();
            randomWec._params = constraints.windSettings;

            if (constraints.customHeights) {
              let height = 0, 
                availableHubHeights = randomWec.availableHubHeights.sort((a,b) => a<b?-1:1),
                maxHubHeight = (constraints.maxTip||300)-randomWec.rotorDiameter/2;
              
              for (var h=0; h<availableHubHeights.length &&
                   availableHubHeights[h]<=maxHubHeight; h++ ) 
                  randomWec._hubHeight = randomWec._params.height = availableHubHeights[h];
            } else {
              randomWec._hubHeight = randomWec._params.height = constraints.maxTip-randomWec.rotorDiameter/2;
            }

            if (randomWec.hubHeight > 0 && 
                (!constraints.maxRotorDiameter || randomWec.rotorDiameter <= constraints.maxRotorDiameter) &&
                (!constraints.maxPower || layout.totalWecKey('ratedPower') <= constraints.maxPower))
                layout.addWec(randomWec);
            attempt += 1;
        } while ((layout.numberOfWecs < (constraints.maxModels||1)) && attempt < maxAttempt);
        if (!layout.wecs.length)
          return null;

        var minIncrement = layout.minPower();
        var power = layout.deratedPower;
        let numberOfWecs = layout.numberOfWecs;
        //console.log(power, minIncrement);
        do {
            var wecNum = Math.floor(Math.random()*layout.wecs.length);
            if (!constraints.maxPower || (power + layout.wecs[wecNum].wec.ratedPower) <= constraints.maxPower) {
                power += layout.wecs[wecNum].wec.ratedPower;
                layout.wecs[wecNum].count = layout.wecs[wecNum].count+1;
                numberOfWecs += 1;
            }
            if ((constraints.maxPower && power+minIncrement > constraints.maxPower) || 
              (constraints.maxNumberOfWecs && constraints.maxNumberOfWecs == numberOfWecs))
                break;
        } while (!constraints.maxPower || power <= constraints.maxPower);
        if (constraints.minNumberOfWecs && numberOfWecs < constraints.minNumberOfWecs)
          return null;
        return layout;
    }

    onAssist(event: any) {
      if (!this.store.wecsSelected.length) {
        this.snackBar.open('No favourite WECs selected.', 'CLOSE');
        return;
      }

      let dialogRef = this.dialog.open(ConstraintsDialog, {
        width: '500px',
        data: this.store.constraints
      });

      let sub = Observable.zip(dialogRef.beforeClose(), this.store.allLayouts, this.store.starredWecs).subscribe( args => {
        let constraints = args[0];
        let layouts : Array<Layout> = <Array<Layout>>(args[1]);
        let newLayouts : Array<Layout> = new Array<Layout>();
        let wecs : Array<Wec> = <Array<Wec>>(args[2]);
        //console.log(args);
        if (!constraints)
          return;

        this.store.constraints = constraints;
        
        if (!wecs.length) {
          this.snackBar.open('No favourite WECs selected.', 'CLOSE');
          return;
        }

        for (let i=0;i<50000;i++){
          let layout : Layout = this.randomLayout(wecs, constraints);
          if (layout && 
            !newLayouts.find( l => l.name == layout.name) && 
            !layouts.find( l => l.name == layout.name)) {
            newLayouts.push(layout);
          }
        }
        newLayouts = newLayouts.sort( (a,b) => (a.deratedPower<b.deratedPower?
         (a.deratedPower==b.deratedPower?(a.netYield<b.netYield?1:-1):-1):-1));
        if (constraints.maxResults)
          newLayouts = newLayouts.slice(0, +constraints.maxResults);
        layouts = layouts.concat(newLayouts);
        layouts.forEach( (l, idx) => l.id = (idx+1));
        this.store.allLayouts = layouts;
      });
      this.subscriptions.push(sub);
    }

    ngOnDestroy() {
      this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

