import { Component, OnInit, OnDestroy, Input, ViewChildren, QueryList, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { CostType, costLabel, costSelect, currencySelect } from '../../consts';

import { Layout, CostItem } from '../../models';
import { DataService } from '../../utils/data.service';
import { Observable, Subscription } from 'rxjs/Rx';
import { NouisliderComponent, NouiFormatter } from 'ng2-nouislider';

class PeriodYearFormatter implements NouiFormatter {
  layout : Layout;
  constructor(layout : Layout) {
    this.layout = layout;
  }
  to(value: number): string {
    // formatting with http://stackoverflow.com/a/26463364/478584
    value = Math.round(value);
    let year = String((this.layout ? this.layout.startOperation : 0) + value);
    return `t<sub>${value.toString()}</sub> (${year})`;
  };

  from(value: string): number {
    return parseInt(value) - (this.layout ? this.layout.startOperation : 0);
  }
}

@Component({
    moduleId: module.id,
    selector: 'wf-layout-cost',
    templateUrl: './wf-layout-cost.component.html',
    styleUrls: ['./wf-layout-cost.component.scss'],
})
export class LayoutCostComponent implements OnInit, OnDestroy {
    @Input() layout: Observable<Layout>;
    @Output() onChange = new EventEmitter<any>(); 

    @ViewChildren(NouisliderComponent) periodSliders: QueryList<NouisliderComponent>;

    currentLayout : Layout;
    costsPerWec: boolean = false;
    costsPerWecChanged: boolean = false;
    costSelect: any = costSelect;
    currencySelect: any = currencySelect;
    costDataGroup: FormGroup;    
    costItems: Array<CostItem> = [];    
    tooltipFormatter : PeriodYearFormatter;
    sliderConfig: any = {
      start: [0, 0],
      step: 1,
      connect: true,
      range: {
        min: 0,
        max: 20,
      }
    };

    subscriptions: Subscription[]=[];

    constructor(
      private store: DataService,
      private formBuilder: FormBuilder) {
    }

    ngOnInit() { 
      let sub1 = this.layout.subscribe( layout => {
        if (!layout) {
          return;
        }
        this.currentLayout = layout;

        this.costDataGroup = this.formBuilder.group({
          startOperation: [layout.startOperation, Validators.required],
          period: [layout.period, Validators.required],
          currency: [layout.currency, Validators.required],
          discountRate: [layout.discountRate, Validators.required],
        });

        this.costItems.splice(0, this.costItems.length);
        for (let idx=0; idx<layout.costItems.length; idx++) {
          let item = layout.costItems[idx];
          this.costItems.push ({
            type: +item.type,
            amount: (+item.amount||0)*(this.costsPerWec?1:layout.numberOfWecs),
            periodRange: item.periodRange,
          });
        }

        this.tooltipFormatter = new PeriodYearFormatter(layout);
        this.sliderConfig.tooltips = [this.tooltipFormatter, this.tooltipFormatter];
        this.sliderConfig.range.max = layout.period;
  
        let sub2 = this.costDataGroup.valueChanges.subscribe(form => {
          let updateSlider = (layout.period != form.period || 
            layout.startOperation != form.startOperation);
          Object.assign(this.currentLayout, form);
          if (updateSlider) this.updateSlider();
          this.updateLayout();
        });
        this.subscriptions.push(sub2);

      });
      this.subscriptions.push(sub1);

    }

    /**
     * Update slider after the view init since this component will
     * be able to query its view for the initialized slider.
     */
    ngAfterViewInit() {
        this.updateSlider();
    }

    updateSlider() {
      this.sliderConfig.range = {
        min: 0, max: this.currentLayout.period
      };
      this.periodSliders.map ( (nouislider, idx) => {
        this.sliderConfig.start = this.currentLayout.costItems[idx].periodRange;
        nouislider.slider.updateOptions(this.sliderConfig) 
      });
    }

    createCostItem() : CostItem {
      return {
        type: CostType.CAPEX,
        amount: 0,
        periodRange: [0 , 0],
      };
    }

    addCostItem() {
      this.costItems.push(this.createCostItem());
    }

    removeCostItem(idx: number, event: any) {
      if (event.detail) /* to prevent enter key firing the event */
        this.costItems.splice(idx, 1);
    }

    onChangeShowCostToggle(event: any) {
      this.costsPerWec = !this.costsPerWec;
      this.costsPerWecChanged = true;

      let layout = this.currentLayout;
      let costsPerWec = this.costsPerWec;
      let qty = layout.numberOfWecs;
      this.costItems.map( item => {
        item.amount = (+item.amount||0)*(this.costsPerWec?1/qty:qty);
      });
      this.updateLayout();
    }

    updateLayout() {
      this.currentLayout.costItems = this.costItems.map( item => {
      let qty = this.currentLayout.numberOfWecs;
        let amountPerWec = (+item.amount||0)*(!this.costsPerWec?1/qty:1);
        return {
          type: item.type, 
          amount: amountPerWec, 
          periodRange: item.periodRange,
        };
      });
      this.onChange.next(this.currentLayout);
    }

    ngOnDestroy() {
      this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

}
