import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ViewChild } from '@angular/core';
import { WecPowerCurveChart } from '../wec-power-curve-chart/wec-power-curve-chart.component';
import { AppToolbarService } from '../../../app-toolbar.service';

import { Wec, Layout, WecGroup, Settings } from '../../models';

import { DataService } from '../../utils/data.service';

import { Observable, Subscription } from 'rxjs/Rx';

@Component({
    moduleId: module.id,
    selector: 'wf-layout-wec-detail',
    templateUrl: './wf-layout-wec-detail.component.html',
    styleUrls: ['./wf-layout-wec-detail.component.scss']
})
export class LayoutWecDetailComponent  implements OnInit, OnDestroy {
    id: number;
    groupId: number;
    layout: Observable<Layout>;
    wec: Observable<Wec>; 

    subscriptions: Subscription[]=[];
    chart : WecPowerCurveChart;

    @ViewChild(WecPowerCurveChart) set wecPowerCurveChart(c : WecPowerCurveChart) {
        this.chart = c;
        this.wec.subscribe( wec => this.chart.setupForWec(wec) );
    }; 

    constructor(
      private route: ActivatedRoute,
      private router: Router,
      private store: DataService,
      private menuService: AppToolbarService) {
    }

    ngOnInit() { 
      this.layout = this.route.params
      .switchMap( args => {
        this.groupId = +args['group']-1;
        return this.store.getLayout(+args['id']);
      }); 
      this.wec = this.layout.map( layout => {
        if (!layout) {
          //this.router.navigate(['/home']);
          return;
        }
        let wec = layout.wecs[this.groupId].wec;
        return wec;
      });
      let merged = Observable.zip(this.layout, this.wec)
        .subscribe( obs => this.menuService.title = `${obs[0].name}: ${obs[1].manufacturer} ${obs[1].model}`);
      this.subscriptions.push(merged);
    }

    onWecSettingsChanged(wec: Wec, newSettings : Settings) {
      if (wec) {
        wec.params = newSettings;
        if (this.chart)
          this.chart.setupForWec(wec);
      }
    }

    ngOnDestroy() {
      this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

}
