import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DataService } from '../../utils/data.service';
import { Wec, WecGroup, Layout, Settings } from '../../models';
import { Observable, Subscription } from 'rxjs/Rx';

@Component({
  selector: 'layout-constraints',
  templateUrl: './wf-layout-constraints.dialog.html',
})
export class ConstraintsDialog {
  title: string;
  settings$: Observable<Settings>;
  settings: Settings;
  constraints: any = { maxResults: 9, customHeights: true}; //{maxPower: 10000, maxTip: 300, maxRotorDiameter: 150, maxModels: 1, maxNumberOfWecs: 10, minNumberOfWecs: 1};
  formConfig: any = {hasHubHeight: false, hasMaxPower: false, columns: 3};
  isLoading : boolean = false;

  constructor(
    public dialogRef: MatDialogRef<ConstraintsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private store : DataService) { 
      this.constraints = Object.assign(this.constraints, data);
      this.settings$ = this.store.settings.do(settings => {
          if (!this.constraints.windSettings) 
              this.constraints.windSettings = settings
      });
      //this.title = data.type == 0 ? 'Log law': 'Power law';
      //this.paramName =  data.type == 0 ? 'z' : 'alpha';
  }

  onSettingsChanged(newSettings : Settings) {
    this.constraints.windSettings = newSettings;
  }

  onExecute(): void {
    this.isLoading = true;
    setTimeout(() => this.dialogRef.close(this.constraints), 100);
  }
}