import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppToolbarService } from '../../../app-toolbar.service';
import { Wec, Layout, WecGroup } from '../../models';

import { DataService } from '../../utils/data.service';

import { Observable, Subscription } from 'rxjs/Rx';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';

@Component({
    moduleId: module.id,
    selector: 'layout-detail',
    templateUrl: './wf-layout-detail.component.html',
    styleUrls: ['./wf-layout-detail.component.scss']
})
export class LayoutDetailComponent  implements OnInit {
    layout: Observable<Layout>; 
    layoutWecGroups: Observable<Array<WecGroup>>; 

    constructor(
      private route: ActivatedRoute,
      private router: Router,
      private store: DataService,
      private menuService: AppToolbarService) {
    }

    ngOnInit() { 
      this.layout = this.route.params.map( (args) => +args['id'])
        .switchMap( id => this.store.getLayout(id) )
        .do( layout => {
          if (!layout)
            this.router.navigate(['layouts']);
          else this.menuService.title = layout.name;
        });
      this.layoutWecGroups = this.layout.map( l => l ? l.wecs : []);
    }

    onLayoutCostsChange(costs) {
      this.layout.first().subscribe( layout => Object.assign(layout, costs));
    }
}
