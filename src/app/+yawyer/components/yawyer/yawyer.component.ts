import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ViewChild } from '@angular/core';
import { WecPowerCurveChart } from '../wec-power-curve-chart/wec-power-curve-chart.component';

import { Wec, Layout, Settings } from '../../models';
import { DataService } from '../../utils/data.service';

import { Router } from '../../../common';

import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable, Subscription } from 'rxjs/Rx';

@Component({
    moduleId: module.id,
    selector: 'yawyer-component',
    templateUrl: './yawyer.component.html',
    styleUrls: ['./yawyer.component.scss']
})
export class YawyerComponent  implements OnInit, OnDestroy {

    
    selectedTabIndex: number = 1;
    showFilter : boolean = false;
    layouts: Array<Layout>;
    hoveredWec: Wec;
    settings: Observable<Settings>;
    allWecs: Observable<Array<Wec>>;
    starredWecs: Observable<Array<Wec>>;

    subscriptions: Subscription[]=[];
    @ViewChild(WecPowerCurveChart) chart : WecPowerCurveChart;

    constructor(
        private store: DataService, 
        private router: Router,
        private snackBar: MatSnackBar) {
        this.settings = this.store.settings;
        this.allWecs = this.store.allWecs;
        this.starredWecs = this.store.starredWecs;
    }

    ngOnInit() { 
        this.selectedTabIndex = this.store.wecsSelected.length==0 ? 1 : 0;
        let sub3 = this.store.allLayouts.subscribe( layouts => this.layouts = layouts );
        this.subscriptions.push(sub3);
    }

    getLastFilter() {
        return this.store.getFilter();
    }

    onSettingsChanged(newSettings : Settings) {
        this.store.settings = Object.assign({}, newSettings);
        if (this.hoveredWec) {
            this.hoveredWec._params = Object.assign({}, newSettings);
            this.chart.setupForWec(this.hoveredWec);
        }
    }

    onWecFilterChanged(currentFilter : any) {
        return this.store.setFilter(currentFilter);
    }
    
    onWecSelected(wec: Wec) {
        this.router.navigate(['browse', wec.id]);
    }

    onWecStarred(wec: Wec) {
        this.store.toggleStarredWec(wec);
    }

    onWecHover(wec: Wec) {
        //console.log(wec);
        this.hoveredWec = wec;
        this.chart.setupForWec(wec);
    }

    onAddToLayout( layout: Layout, wec: Wec) {
      if (!layout) {
        this.store.addLayout().subscribe( layout => {
            layout.addWec(wec.copy());
            this.snackBar.open(`Wec ${wec.model} added to layout #${layout.id}`, 'OK', { duration: 2000 });
        });
      } else {
          layout.addWec(wec.copy());
          this.snackBar.open(`Wec ${wec.model} added to layout #${layout.id}`, 'OK', { duration: 2000 });
      }
    }

    ngOnDestroy() {
      this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

}
