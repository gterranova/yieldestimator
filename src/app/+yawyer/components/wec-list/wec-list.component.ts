import { Component, OnInit, ViewContainerRef, Input, Output, EventEmitter } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Sort } from '@angular/material';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { Observable, Subscription } from 'rxjs/Rx';
import { DataService } from '../../utils/data.service';
import { MatMenu } from '@angular/material';

import { Wec } from '../../models';
import { BaseList } from '../common/base-list.component';

@Component({
    moduleId: module.id,
    selector: 'wec-list',
    templateUrl: './wec-list.component.html',
    styleUrls: ['./wec-list.component.scss']
})
export class WecList extends BaseList<Wec> implements OnInit, OnDestroy {
    @Input() showFilter: boolean = false;
    @Input() cartMenu: MatMenu;     
    @Output() onHover = new EventEmitter<Wec>(); 
    @Output() onStar = new EventEmitter<Wec>(); 
    @Output() onCart = new EventEmitter<Wec>(); 
    @Output() onSelect = new EventEmitter<Wec>(); 

    displayedColumns = ['select','model', 'deratedPower', 'hubHeight', 'velocityHubHeight',
      'grossYield', 'netYield', 'addToCart'];

    defaultSort: Sort = <Sort>{active: 'grossYield', direction: 'desc'};

    // MatPaginator Inputs
    length = 9;
    pageSize = 9;
    pageSizeOptions = [9, 25, 100];

    constructor() { 
      super();
    }

    ngOnInit() {
    }

    setPageSizeOptions(setPageSizeOptionsInput: string) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }

    filterPredicate(data: Wec, filter: string) : boolean {
      let result = true;
      let params = JSON.parse(filter);
      if (params.fullText)
        result = result && (data.manufacturer+" "+data.model).trim().toLowerCase().indexOf(params.fullText.trim().toLowerCase()) != -1;
      if (result && params.minRatedPower)
        result = result && data.ratedPower >= params.minRatedPower;
      if (result && params.maxRatedPower)
        result = result && data.ratedPower <= params.maxRatedPower;
      if (result && params.minTipHeight)
        result = result && data.tipHeight >= params.minTipHeight;
      if (result && params.maxTipHeight)
        result = result && data.tipHeight <= params.maxTipHeight;
      if (result && params.minRotorDiameter)
        result = result && data.rotorDiameter >= params.minRotorDiameter;
      if (result && params.maxRotorDiameter)
        result = result && data.rotorDiameter <= params.maxRotorDiameter;
      if (result)
        result = result && data.hubHeight > (data.rotorDiameter / 2);
      return result;
    }

    sortPredicate(sort: Sort, a: Wec, b: Wec) : number {
      let isAsc = sort.direction == 'asc';
      if (a.favourite !== b.favourite) {
        return compare(a.favourite?1:0, b.favourite?1:0, false);
      }
      if (sort.active == 'model')
        return compare(a[sort.active], b[sort.active], isAsc);
      return compare(+a[sort.active], +b[sort.active], isAsc);
    }

    onStarClicked(wec: Wec, event: any) {
      if (event)
        event.stopPropagation();
      this.onStar.next(wec);
      this.data.data = this.sortData(this._items);
    }

    onCartClicked(wec: Wec, event: any) {
      if (event)
        event.stopPropagation();
      this.onCart.next(wec);
    }

    onAdjustV(wec: Wec, amount: number, event: any) {
      if (event)
        event.stopPropagation();
      wec.hubHeight += amount;
    }

    onSelectRow(wec: Wec) {
      this.onSelect.next(wec);
    }

    onHoverRow(wec: Wec) {
      this.onHover.next(wec);
    }

}


function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}