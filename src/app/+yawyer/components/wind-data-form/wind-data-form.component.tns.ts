import { Component, OnInit, ViewContainerRef, Input, Output, EventEmitter } from '@angular/core';
import { ListPicker } from "ui/list-picker";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";

import { WindExtrapolationDialogContent } from './wind-data-form.extrapolation-dialog';
//import { ROUGHNESS_CLASSES, HELLMAN_EXPONENTS, initialParams } from '../../consts';
//import { HellmannExponent } from '../../models/hexponent';
//import { RoughnessClass } from '../../models/roughness';

@Component({
    moduleId: module.id,
    selector: 'wind-data-form',
    templateUrl: './wind-data-form.component.html',
    styleUrls: ['./wind-data-form.component.scss']
})
export class WindDataForm implements OnInit {
    @Input() params?: any = initialParams;
    @Output() onParamsChange = new EventEmitter<any>();

    extrapolationModeLabel: string;

    constructor(private modalService: ModalDialogService, private viewContainerRef: ViewContainerRef) {
    }

    public selectExtrapolationMode() {
        let selectedIndex = this.params.mode=="1"?
            ROUGHNESS_CLASSES.find((i) => this.params.z == i.z).id-1:
            HELLMAN_EXPONENTS.find((i) => this.params.alpha == i.alpha).id-1;

        let options: ModalDialogOptions = {
            context: { 
                promptMsg: "Select wind extrapolation mode",
                roughnessClasses: ROUGHNESS_CLASSES.map((rc) => rc.title),
                hellmannExp: HELLMAN_EXPONENTS.map((h) => h.title),
                selectedMode: this.params.mode,
                selectedIndex: selectedIndex
            },
            fullscreen: true,
            viewContainerRef: this.viewContainerRef
        };

        this.modalService.showModal(WindExtrapolationDialogContent, options)
            .then((dialogResult: any) => {
              //console.log(JSON.stringify(dialogResult));
              if (dialogResult.selectedMode == "1") {
                  this.params.mode = dialogResult.selectedMode;
                  this.params.z = ROUGHNESS_CLASSES[dialogResult.selectedIndex].z;
              } else if (dialogResult.selectedMode == "2") {
                  this.params.mode = dialogResult.selectedMode;
                  this.params.alpha = HELLMAN_EXPONENTS[dialogResult.selectedIndex].alpha;
              }
              this.updateExtrapolationLabel();
            });
    }

    private updateExtrapolationLabel() {
      if (this.params.mode == "1") {
          this.extrapolationModeLabel = "Log Law (z = "+this.params.z+")";
      } else if (this.params.mode == "2") {
          this.extrapolationModeLabel = "Power Law (alpha = "+this.params.alpha+")";
      }        
    }

    calc() {
      if (this.onParamsChange)
          this.onParamsChange.emit(this.params);
    }

    ngOnInit() {
        this.updateExtrapolationLabel();
    }

}
