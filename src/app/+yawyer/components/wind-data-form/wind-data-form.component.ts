import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, Inject } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { windExtrapolationModes } from '../../consts';
import { Settings } from '../../models';
import { Subscription } from 'rxjs/Rx';

interface FormConfig {
    hasHubHeight: boolean;
    hasMaxPower: boolean;
    columns: number;
}

@Component({
    moduleId: module.id,
    selector: 'wind-data-form',
    templateUrl: './wind-data-form.component.html',
    styleUrls: ['./wind-data-form.component.scss']
})
export class WindDataForm implements OnInit, OnDestroy {
    @Input() params : Settings; 
    @Input() config : FormConfig = {hasHubHeight: true, hasMaxPower: true, columns: 4}; 
    @Output() onParamsChanged = new EventEmitter<Settings>(); 

    paramForm: FormGroup;
    currentWEM: any;
    windExtrapolationModes : any = windExtrapolationModes;
    subscriptions: Subscription[]=[];

    constructor(
        private formBuilder: FormBuilder,
        public dialog: MatDialog) {
    }

    ngOnInit() {
        this.currentWEM = Object.assign({}, this.params.wem);
        let selectedWEM = !this.isWEMValueCustom(this.currentWEM) ? this.currentWEM : {type: this.currentWEM.type, value: -99};
        this.paramForm = this.formBuilder.group({
            vmast_hh: [this.params.vmast_hh, [Validators.required]],
            hmast_hh: [this.params.hmast_hh, Validators.required],
            height: [this.params.height, Validators.required],
            k: [this.params.k, Validators.required],
            wem: [this.o2v(selectedWEM.type, selectedWEM.value), Validators.required],
            rho: [this.params.rho, Validators.required],
            max_power: [this.params.max_power, Validators.required],
            losses: [this.params.losses, Validators.required],
            availability: [this.params.availability, Validators.required],
        });
        let sub2 = this.paramForm.valueChanges.subscribe(form => {
            form.wem = this.currentWEM;
            this.onParamsChanged.next(form);
            this.paramForm.updateValueAndValidity({emitEvent: false});
        });
        this.subscriptions.push(sub2);
    }

    isWEMValueCustom(wem: any) {
        if (wem.value == -99)
            return true;
        let values = windExtrapolationModes.find( mode => mode.id == wem.type).values;
        return values.find( opt => opt.value == wem.value) == null;
    }

    o2v(type, value) {
        return JSON.stringify({ type, value});
    }

    v2o(value) {
        return JSON.parse(value);
    }

    OnWEMChange(event) {
        let selectedWEM = this.v2o(event.value);
        if (selectedWEM.value == -99) {
            let dialogRef = this.dialog.open(CustomWEMDialog, {
              width: '250px',
              data: { type: selectedWEM.type, value: selectedWEM.type == this.currentWEM.type ? this.currentWEM.value: 0 }
            });

            dialogRef.afterClosed().subscribe(result => {
                let validOrNot = this.currentWEM = result||this.currentWEM;
                let selectedWEM = !this.isWEMValueCustom(validOrNot) ? validOrNot : {type: validOrNot.type, value: -99};
                this.paramForm.controls['wem'].patchValue(this.o2v(selectedWEM.type, selectedWEM.value));
            });
        } else {
            this.currentWEM = selectedWEM;
            this.paramForm.controls['wem'].patchValue(this.o2v(selectedWEM.type, selectedWEM.value));
        }
    }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

}

@Component({
  selector: 'custom-wem-dialog',
  template: `<h2 mat-dialog-title>{{ title }}</h2>
<mat-dialog-content>
    Please enter a custom value for parameter {{ paramName }}:
    <mat-form-field class="full-width"><input (keyup.enter)="onEnter()" matInput type="number" step="any" [(ngModel)]="data.value"></mat-form-field>
</mat-dialog-content>
<mat-dialog-actions>
  <button mat-button mat-dialog-close>Cancel</button>
  <button mat-button class="mat-raised-button mat-primary" [mat-dialog-close]="data">Ok</button>
</mat-dialog-actions>
  `,
})
export class CustomWEMDialog {
  title: string;
  paramName: string;

  constructor(
    public dialogRef: MatDialogRef<CustomWEMDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 

      this.title = data.type == 0 ? 'Log law': 'Power law';
      this.paramName =  data.type == 0 ? 'z' : 'alpha';
  }

  onEnter(): void {
    this.dialogRef.close(this.data);
  }

}
