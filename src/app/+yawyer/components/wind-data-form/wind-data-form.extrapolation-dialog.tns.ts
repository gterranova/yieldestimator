import { Component } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { SegmentedBar, SegmentedBarItem } from "ui/segmented-bar";
import { ListPicker } from "ui/list-picker";

@Component({
    selector: "modal-content",
    template: `
    <GridLayout rows="80,auto,auto">
        <StackLayout row="0" verticalAlignment="center" horizontalAlignment="center" class="header">
            <Label [text]="prompt" class="title" textWrap="true"></Label>
        </StackLayout>
        <StackLayout row="1" verticalAlignment="center">
            <SegmentedBar [items]="extrapolationModes" [selectedIndex]="selectedModeIndex" (selectedIndexChange)="changeAction($event)" class="m-5"></SegmentedBar>
            <Label
                [text]="selectedMode == '1' ? 'Use roughness' : 'Use Hellmann-Exponent'"
                textWrap="true" class="h3 m-15"></Label>

            <ListPicker *ngIf="selectedMode=='1'" [items]="roughnessClasses" [selectedIndex]="selectedIndex" (selectedIndexChange)="selectedRoughnessIndexChanged($event)" row="1"></ListPicker>
            <ListPicker *ngIf="selectedMode=='2'" [items]="hellmannExp" [selectedIndex]="selectedIndex" (selectedIndexChange)="selectedHellmannIndexChanged($event)" row="1"></ListPicker>

        </StackLayout>
        <StackLayout row="2" horizontalAlignment="center" orientation="horizontal" marginTop="12">
            <Button text="ok" (tap)="close('OK')" class="btn btn-primary btn-active"></Button>
            <Button text="cancel" (tap)="close('Cancel')" class="btn btn-secondary btn-active"></Button>
        </StackLayout>
    </GridLayout>
  `,
  styles: [`
      .header { background-color: #efefef; width: 100%; height: 80px; text-align: center; }
      .title { height: 70px; background-color: #efefef; font-size: 18px; color: #000000}
  `]
})
export class WindExtrapolationDialogContent {
    public prompt: string;
    roughnessClasses: string[];
    hellmannExp: string[];
    extrapolationModes: Array<SegmentedBarItem>;
    selectedMode: string ="1";
    selectedModeIndex: number = 0;
    selectedIndex: number = 0;

    constructor(private params: ModalDialogParams) {
        this.prompt = params.context.promptMsg;
        this.roughnessClasses = params.context.roughnessClasses;
        this.hellmannExp = params.context.hellmannExp;
        this.selectedMode = params.context.selectedMode;
        this.selectedModeIndex = this.selectedMode == "1"? 0 : 1;
        this.selectedIndex = params.context.selectedIndex;        
        this.extrapolationModes = [];
        const item0 = new SegmentedBarItem();
        item0.title = "Log law";
        const item1 = new SegmentedBarItem();
        item1.title = "Power law";
        this.extrapolationModes.push(item0);     
        this.extrapolationModes.push(item1);     

    }

    changeAction(args){
        let segmentedBar = <SegmentedBar>args.object;
        this.selectedMode = segmentedBar.selectedIndex == 0 ? "1" : "2";
    }

    public selectedRoughnessIndexChanged(args) {
        let picker = <ListPicker>args.object;
        this.selectedIndex = picker.selectedIndex;
    }

    public selectedHellmannIndexChanged(args) {
        let picker = <ListPicker>args.object;
        this.selectedIndex = picker.selectedIndex;
    }

    public close(result: string) {
        if (result == 'OK') 
            this.params.closeCallback({
                selectedMode: this.selectedMode, 
                selectedIndex: this.selectedIndex
            });
        else 
            this.params.closeCallback({
                selectedMode: ""
            });
  }
}