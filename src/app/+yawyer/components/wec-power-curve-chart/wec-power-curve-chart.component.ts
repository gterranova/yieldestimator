import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../../utils/data.service';
import { Subscription } from 'rxjs/Rx';
import { trigger, style, animate, transition } from '@angular/animations';

import { Wec } from '../../models';

@Component({
  moduleId: module.id,  
  selector: 'wec-power-curve-chart',
  templateUrl: './wec-power-curve-chart.component.html',
  styleUrls: ['./wec-power-curve-chart.component.scss']
})
export class WecPowerCurveChart implements OnInit, OnDestroy {
  wec: Wec;
  dataSource: Array<any> = new Array<any>();
  
  subscriptions: Subscription[]=[];
  
  @Input() showData: boolean;  

  constructor(private store: DataService) {
  }

  ngOnInit() {
    //this.setupForWec(null);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  setupForWec(wec: Wec) {
    if (!wec) {
      setTimeout( () => {
        let _lineChartData = new Array();
        _lineChartData = [
          {data: Array.from(Array(30).join('0').split('').map(parseInt)), label: 'Wind h', yAxisID: 'yAxis1'},
          {data: Array.from(Array(30).join('0').split('').map(parseInt)), label: 'Power', yAxisID: 'yAxis2'},
        ];      
        this.lineChartData = _lineChartData;
      }, 0);
      return; 
    }
    setTimeout( () => { return this.wec = wec; }, 0);
    let Pels = wec.ratedYieldCurve;
    let Hs = wec.extrapolatedYieldCurve;
    if (this.showData) {
        let Cp = wec.Cp;
        this.dataSource.splice(0, this.dataSource.length);
        for (let i=0; i<30; i++) {
          this.dataSource.push({
             V: i,
             Cp: Cp[i], 
             Pel: Pels[i],
             h: Hs[i]
          });
        }
    }

    let _lineChartData = new Array();
    _lineChartData.push({ data: Hs, label: "Wind h", yAxisID: 'yAxis1' });
    _lineChartData.push({ data: Pels, label: "Power", yAxisID: 'yAxis2' });
    this.lineChartData = _lineChartData;    
  }
  public lineChartData:Array<any> = [
    {data: Array.from(Array(30).join('0').split('').map(parseFloat)), label: 'Wind h', yAxisID: 'yAxis1'},
    {data: Array.from(Array(30).join('0').split('').map(parseFloat)), label: 'Power', yAxisID: 'yAxis2'},
  ];
  public lineChartLabels:Array<any> = Array.from(Array(30).keys());

  public lineChartOptions:any = {
    responsive: true,
    scales: {
      yAxes: [
          {
              id: 'yAxis1',
              ticks: {
                  beginAtZero:true,
                  step: 100,
                  max: 1200
              },
              position:'left'
          },
          {
              id: 'yAxis2',
              ticks: {
                  beginAtZero:true,
                  step: 200,
                  max: 6500
              },
              position:'right'
          }
      ],
    },
    maintainAspectRatio: true,
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(255,159,177,1)',
      pointBackgroundColor: 'rgba(255,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';

  // events
  public chartClicked(e:any):void {
    //console.log(e);
  }

  public chartHovered(e:any):void {
    //console.log(e);
  }
}