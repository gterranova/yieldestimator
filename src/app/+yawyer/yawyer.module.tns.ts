import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SHARED_MODULES, COMPONENT_DECLARATIONS, COMPONENT_EXPORTS } from './yawyer.common';
import { WindExtrapolationDialogContent } from './components/wind-data-form/wind-data-form.extrapolation-dialog';

@NgModule({
    imports: [
        ...SHARED_MODULES,
    ],
    declarations: [
        WindExtrapolationDialogContent,
        ...COMPONENT_DECLARATIONS
    ],
    entryComponents: [
        WindExtrapolationDialogContent,
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
        exports: [
        ...COMPONENT_EXPORTS
    ]
})
export class YawyerModule { }
