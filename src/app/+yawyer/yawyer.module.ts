import { NgModule } from '@angular/core';
import { SHARED_MODULES, COMPONENT_DECLARATIONS, COMPONENT_EXPORTS } from './yawyer.common';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { DataService } from './utils/data.service';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NouisliderModule } from 'ng2-nouislider';
import { CustomWEMDialog } from './components/wind-data-form/wind-data-form.component';
import { ConstraintsDialog } from './components/wf-configurator/wf-layout-constraints.dialog';

@NgModule({
    imports: [
        MaterialModule,
        ReactiveFormsModule,
        ChartsModule,
        FlexLayoutModule,
        NouisliderModule,
        ...SHARED_MODULES,
    ],
    declarations: [
        ...COMPONENT_DECLARATIONS,
        CustomWEMDialog,
        ConstraintsDialog,
    ],
    exports: [
        ...COMPONENT_EXPORTS
    ],
    providers: [
        DataService
    ],
    entryComponents: [
        CustomWEMDialog,
        ConstraintsDialog,
    ]
})
export class YawyerModule { }
