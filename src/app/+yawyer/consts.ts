
export const enum CostType { 
  CAPEX, OPEX, OtherCAPEX
};

export const costLabel = {
  [CostType.CAPEX]: 'CAPEX',
  [CostType.OtherCAPEX]: 'Other Investments costs',
  [CostType.OPEX]: 'OPEX'
};

export const costSelect = [
    {
      name: 'CAPEX',
      values: [
        { value: CostType.CAPEX, title: costLabel[CostType.CAPEX]},
        { value: CostType.OtherCAPEX, title: costLabel[CostType.OtherCAPEX]},
      ]
    },{
      name: 'OPEX',
      values: [
        { value: CostType.OPEX, title: costLabel[CostType.OPEX]},
      ]
    }
];

export enum Currencies { EUR, AUS, CAN, GB, INR, USD };

export const enum Countries { 
  Australia, Austria, Belgium, Bulgaria, Canada, Chile, Egypt, Finland, France, Germany,
  Greece, Hungary, India, Italy, Japan, Mexico, Netherlands, Norway, 
  Offshore_Belgium, Offshore_Germany, Offshore_Others, Offshore_UK,
  Others, Poland, Portugal, Romania, Spain, Sweden, Switzerland, Turkey, UK, USA };

export const currencySelect = [
  { value: Currencies.EUR, title: 'EUR'},
  { value: Currencies.AUS, title: 'AUS'},
  { value: Currencies.CAN, title: 'CAN'},
  { value: Currencies.GB, title: 'GB'},
  { value: Currencies.INR, title: 'INR'},
  { value: Currencies.USD, title: 'USD'},
];
 
export const countrySelect = [
    {
      name: 'Onshore',
      values: [
        { value: Countries.Australia, title: 'Australia'},
        { value: Countries.Austria, title: 'Austria'},
        { value: Countries.Belgium, title: 'Belgium'},
        { value: Countries.Bulgaria, title: 'Bulgaria'},
        { value: Countries.Canada, title: 'Canada'},
        { value: Countries.Chile, title: 'Chile'},
        { value: Countries.Egypt, title: 'Egypt'},
        { value: Countries.Finland, title: 'Finland'},
        { value: Countries.France, title: 'France'},
        { value: Countries.Germany, title: 'Germany'},
        { value: Countries.Greece, title: 'Greece'},
        { value: Countries.Hungary, title: 'Hungary'},
        { value: Countries.India, title: 'India'},
        { value: Countries.Italy, title: 'Italy'},
        { value: Countries.Japan, title: 'Japan'},
        { value: Countries.Mexico, title: 'Mexico'},
        { value: Countries.Netherlands, title: 'Netherlands'},
        { value: Countries.Norway, title: 'Norway'},
        { value: Countries.Poland, title: 'Poland'},
        { value: Countries.Portugal, title: 'Portugal'},
        { value: Countries.Romania, title: 'Romania'},
        { value: Countries.Spain, title: 'Spain'},
        { value: Countries.Sweden, title: 'Sweden'},
        { value: Countries.Switzerland, title: 'Switzerland'},
        { value: Countries.Turkey, title: 'Turkey'},
        { value: Countries.UK, title: 'United Kingdom'},
        { value: Countries.USA, title: 'United States'},
        { value: Countries.Others, title: 'Other'},
      ]
    },
    {
      name: 'Offshore',
      values: [
        { value: Countries.Offshore_Belgium, title: 'Belgium'},
        { value: Countries.Offshore_Germany, title: 'Germany'},
        { value: Countries.Offshore_UK, title: 'United Kingdom'},
        { value: Countries.Offshore_Others, title: 'United States'},
      ]
    }
];

export const windExtrapolationModes = [
    {
      id: 0,
      name: 'Log Law',
      values: [
        { value: 0.0002, title: 'Class 0: Water surface'},
        { value: 0.0024, title: 'Class 0.5: Completely open terrain with a smooth surface, e.g. mowed grass'},
        { value: 0.03, title: 'Class 1: Open agricultural area without fences and hedgerows, only softly rounded hills'},
        { value: 0.055, title: 'Class 1.5: Agricultural land with some houses and 8m tall hedgerows with a distance of ~1,25km'},
        { value: 0.1, title: 'Class 2: Agricultural land with some houses and 8m tall hedgerows with a distance of ~500m'},
        { value: 0.2, title: 'Class 2.5: Agricultural land with many houses and plants, or 8m tall hedgerows with a distance of ~250m'},
        { value: 0.4, title: 'Class 3: Villages, agricultural land with many or tall sheltering hedgerows, forests and very rough and uneven terrain'},
        { value: 0.8, title: 'Class 3.5: Larger cities with tall buildings'},
        { value: 1.6, title: 'Class 4: Very large cities with tall buildings and skycrapers'},
        { value: -99, title: 'Custom z value (user input)'},
      ]
    },
    {
      id: 1,
      name: 'Power Law',
      values: [
        { value: 0.06, title: 'Unstable air above open water surface'},
        { value: 0.1, title: 'Neutral air above open water surface'},
        { value: 0.11, title: 'Unstable air above flat open coast'},
        { value: 0.16, title: 'Neutral air above flat open coast'},
        { value: 0.27, title: 'Stable air above open water surface'},
        { value: 0.27, title: 'Unstable air above human inhabited areas'},
        { value: 0.34, title: 'Neutral air above human inhabited areas'},
        { value: 0.4, title: 'Stable air above flat open coast'},
        { value: 0.6, title: 'Stable air above human inhabited areas'},
        { value: -99, title: 'Custom alpha value (user input)'},
      ]
    }
];
